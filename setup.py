#!/usr/bin/env python
from distutils.core import setup
from setuptools import setup
import versioneer
setup(name='pybbdes',
      version=versioneer.get_version(),
      cmdclass=versioneer.get_cmdclass(),
      description='tools to decrypt des encrypted file',
      author='Huiliang Su',
      author_email='suhuiliang@gmai.com',
      url='',
      packages=['pybbdes'],
      package_dir={'': 'src'},
      install_requires=['pycryptodomex'],
      setup_requires=['pytest-runner'],
      tests_require=['pytest'],
      test_suite="test",
      license='MIT',
      classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.6',
      ],
      )
