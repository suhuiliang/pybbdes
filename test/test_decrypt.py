from pathlib import Path

from pytest import fixture

from pybbdes.decrypt import decrypt


class TestDecrypt(object):

    def test_decrypt(self, resources_dir: Path):
        key = "birthday"
        input_path = (resources_dir / 'test_file_encrypted').resolve()

        output_path = Path("./out/result.txt").resolve()
        output_path.parent.mkdir(parents=True, exist_ok=True)

        assert input_path.exists()
        decrypt(key, str(input_path), str(output_path), uu_encoded=True)
        assert output_path.exists()

        with open(str(output_path), 'rb') as out_file:
            first_line = out_file.readline()
            assert first_line.strip(b' \r\n') == b'"hello world"'


@fixture
def resources_dir(request):
    return Path(request.config.rootdir, "resources")
