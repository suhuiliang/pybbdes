from pytest import raises

from pybbdes.des_tools import str2bytes, get_left_shift_bytes, des_cbc_checksum


class TestDesTools(object):

    def test_str2bytes(self):
        in_str = "abc"
        result = str2bytes(in_str)
        assert type(result) is bytes
        assert result == bytes([0x61, 0x62, 0x63])

    def test_should_handle_bytes(self):
        in_b = b"abc"
        with raises(TypeError):
            str2bytes(in_b)

    def test_get_left_shifted_bytes(self):
        result = get_left_shift_bytes("birthday")
        assert type(result) is bytes
        assert result == bytes([0xc4, 0xd2, 0xe4, 0xe8, 0xd0, 0xc8, 0xc2, 0xf2])

    def test_get_checksum(self):
        op = bytes([0xc4, 0xd3, 0xe5, 0xe9, 0xd0, 0xc8, 0xc2, 0xf2])
        result = des_cbc_checksum("birthday", op, op)
        assert type(result) is bytes
        assert result == bytes([0x49, 0x2a, 0xd1, 0x9b, 0x9d, 0x03, 0xfa, 0xb8])
