pybbdes
==========


The idea of this lib is to decrypt DES encrypted file from Bloomberg without using provided windows application.
It is inspirited by https://github.com/humblejok/jok_des_tools

Run
----------

The entry point is pybbdes/decrypt.py

.. code:
    python -m pybbdes.decrypt -k "xxxxxxx" -i ~/newIssueTest_1804161430 -u

Dev env
---------

Install pyenv

pyenv install 3.6.4
pyenv install 3.5.5

pyenv global 3.6.4 3.5.5


Style check
-------------
make lint

Features
--------

* TODO

- Finalize this project. It is only a POC stage now.


