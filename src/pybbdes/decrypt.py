#!/usr/bin/env python3
import logging
from pathlib import Path

from Cryptodome.Cipher import DES

from .des_tools import des_string_to_key
from .uudecode import uu_decode

log = logging.getLogger(__name__)


def decrypt(key: str, in_filename, out_filename=None, uu_encoded=False):
    des_key = des_string_to_key(key)
    iv = bytearray([0] * 8)
    des = DES.new(des_key, mode=DES.MODE_CBC, iv=bytes(iv))

    with open(in_filename, 'rb') as in_file:
        s = in_file.read()
        if uu_encoded:
            s, _ = uu_decode(s)
        decrypted = des.decrypt(s)
        if out_filename:
            p = Path(out_filename)
            p.parent.mkdir(parents=True, exist_ok=True)
            log.info("Write to {}".format(out_filename))
            with open(out_filename, 'wb') as of:
                of.write(decrypted)
        else:
            print(decrypted.decode('cp1252'))


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('-k', '--key', dest='key', required=True, help='The key used to decrypt file')
    parser.add_argument('-i', '--in', dest='in_filename', required=True, help='File need to be decrypted.')
    parser.add_argument('-o', '--out', dest='out_filename', help='The output file')
    parser.add_argument('-u', dest='uu_encoded', default=False, action='store_true',
                        help="Use this if input file is uu encoded")

    args = parser.parse_args()
    decrypt(args.key, args.in_filename, args.out_filename, uu_encoded=args.uu_encoded)
