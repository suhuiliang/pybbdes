from io import BytesIO

import binascii

"""
The python native solution cannot handle '\r\n', so a quick patch is here.
"""


def uu_decode(input_bytes, errors='strict'):
    assert errors == 'strict'
    infile = BytesIO(input_bytes)
    outfile = BytesIO()
    readline = infile.readline
    write = outfile.write

    # Find start of encoded data
    while 1:
        s = readline()
        if not s:
            raise ValueError('Missing "begin" line in input data')
        if s[:5] == b'begin':
            break

    # Decode
    while True:
        s = readline()
        if not s or s.strip(b' \t\r\n\f') == b'end':
            break
        try:
            data = binascii.a2b_uu(s)
        except binascii.Error as v:
            nbytes = (((s[0] - 32) & 63) * 4 + 5) // 3
            data = binascii.a2b_uu(s[:nbytes])
        write(data)
    if not s:
        raise ValueError('Truncated input data')

    return outfile.getvalue(), len(input_bytes)
