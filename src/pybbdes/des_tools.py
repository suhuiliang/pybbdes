# -*- coding: utf-8 -*-
from Cryptodome.Cipher import DES

from typing import TypeVar

ByteType = TypeVar('ByteType', bytes, bytearray)

#
# Predefined odd parity table using to convert byte arrays
#
ODD_PARITY_TABLE = [
    1, 1, 2, 2, 4, 4, 7, 7, 8, 8, 11, 11, 13, 13, 14, 14,
    16, 16, 19, 19, 21, 21, 22, 22, 25, 25, 26, 26, 28, 28, 31, 31,
    32, 32, 35, 35, 37, 37, 38, 38, 41, 41, 42, 42, 44, 44, 47, 47,
    49, 49, 50, 50, 52, 52, 55, 55, 56, 56, 59, 59, 61, 61, 62, 62,
    64, 64, 67, 67, 69, 69, 70, 70, 73, 73, 74, 74, 76, 76, 79, 79,
    81, 81, 82, 82, 84, 84, 87, 87, 88, 88, 91, 91, 93, 93, 94, 94,
    97, 97, 98, 98, 100, 100, 103, 103, 104, 104, 107, 107, 109, 109, 110, 110,
    112, 112, 115, 115, 117, 117, 118, 118, 121, 121, 122, 122, 124, 124, 127, 127,
    128, 128, 131, 131, 133, 133, 134, 134, 137, 137, 138, 138, 140, 140, 143, 143,
    145, 145, 146, 146, 148, 148, 151, 151, 152, 152, 155, 155, 157, 157, 158, 158,
    161, 161, 162, 162, 164, 164, 167, 167, 168, 168, 171, 171, 173, 173, 174, 174,
    176, 176, 179, 179, 181, 181, 182, 182, 185, 185, 186, 186, 188, 188, 191, 191,
    193, 193, 194, 194, 196, 196, 199, 199, 200, 200, 203, 203, 205, 205, 206, 206,
    208, 208, 211, 211, 213, 213, 214, 214, 217, 217, 218, 218, 220, 220, 223, 223,
    224, 224, 227, 227, 229, 229, 230, 230, 233, 233, 234, 234, 236, 236, 239, 239,
    241, 241, 242, 242, 244, 244, 247, 247, 248, 248, 251, 251, 253, 253, 254, 254]


def _mask(n):
    """Return a bitmask of length n (suitable for masking against an
       int to coerce the size to a given length)
    """
    if n >= 0:
        return 2 ** n - 1
    else:
        return 0


def _ror(n, rotations=1, width=8):
    """Return a given number of bitwise right rotations of an integer n,
       for a given bit field width.
    """
    rotations %= width
    if rotations < 1:
        return n
    n &= _mask(width)
    return (n >> rotations) | ((n << (width - rotations)) & _mask(width))


def str2bytes(input_str: str):
    return bytes(input_str, 'utf-8')


def des_cbc_checksum(s_key: str, b_key: bytes, iv: bytes = "\0\0\0\0\0\0\0\0") -> bytes:
    """
    Generate the CBC checksum.
    It has no use by itself: it is part of a key conversion process.

    :param s_key: Key as string object
    :param b_key: Key as byte array
    :param iv:  Initial vector as byte array
    :return:
    """
    cypher = DES.new(b_key, DES.MODE_CBC, iv)
    str_key_in_bytes = str2bytes(s_key)
    encrypted_key = cypher.encrypt(str_key_in_bytes)

    checksum = bytearray(8)
    start_at = len(encrypted_key) - 8
    for index in range(0, 8):
        checksum[index] = encrypted_key[start_at + index]
    return bytes(checksum)


def des_string_to_key(str_key: str) -> bytes:
    """
    Convert a DES string key as a bytes array
    Inspired from: http://web.mit.edu/macdev/Development/MITKerberos/MITKerberosLib/DESLib/Documentation/api.html
    """
    left_shifted_bytes = get_left_shift_bytes(str_key)
    odd_parity_key = set_odd_parity(left_shifted_bytes)
    # Computing CBC checksum (generating key)
    cbc_checksum = des_cbc_checksum(str_key, odd_parity_key, odd_parity_key)

    # Getting odd parity
    result = set_odd_parity(cbc_checksum)
    return result


def get_left_shift_bytes(str_key: str) -> bytes:
    key = bytearray(8)
    for index in range(0, len(str_key)):
        b_val = ord(str_key[index]) & 0xff
        if (index % 16) < 8:
            key[index % 8] = key[index % 8] ^ (b_val << 1)
        else:
            b_val = ((b_val << 4) & 0xf0) | (_ror(b_val, 4) & 0x0f)
            b_val = ((b_val << 2) & 0xcc) | (_ror(b_val, 2) & 0x33)
            b_val = ((b_val << 1) & 0xaa) | (_ror(b_val, 1) & 0x55)
            key[7 - (index % 8)] ^= b_val
        index += 1
    return bytes(key)


def set_odd_parity(bytes_key: ByteType) -> bytes:
    """
    Apply the odd parity table to the given byte array
    """
    return bytes([ODD_PARITY_TABLE[c] for c in bytes_key])
